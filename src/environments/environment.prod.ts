export const environment = {
  production: true,

  authenticationApiURl: 'https://users.ws.montpellier.epsi.fr/api/auth',
  usersApiURl: 'https://users.ws.montpellier.epsi.fr/api/users',
  webStorageApiUrl: 'https://storage.ws.montpellier.epsi.fr/api/folders',

  databaseAccountApiURl: 'https://database.ws.montpellier.epsi.fr/api/accounts',
  databaseApiURl: 'https://database.ws.montpellier.epsi.fr/api/databases',
  contributorApiURl: 'https://database.ws.montpellier.epsi.fr/api/contributors'
};
